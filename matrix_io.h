//
//  matrix_io.h
//  MatrixIO
//
//  Satyam Ghodasara
//

#ifndef MATRIX_IO_H
#define MATRIX_IO_H

#include "mat.h"

//  Reading from matrix files
mxArray *getMatrixInMATFile(const char *fileName, const char *matrixName);
mxArray *getMatrixInHDF5File(const char *fileName, const char *matrixName);
mxArray *getColInMatrix(const mxArray *matrix, const int colIdx);
mxArray *getRowInMatrix(const mxArray *matrix, const int rowIdx);

//  Writing matrix files
int putMatrixInMATFile(const char *fileName, const char *matrixName, const mxArray *matrix);
int putMatrixInHDF5File(const char *fileName, const char *matrixName, const mxArray *matrix);

#endif
