CC= gcc
RM= rm -vf
CFLAGS= -Wall -Wextra -g -I/Applications/MATLAB_R2016a.app/extern/include
CPPFLAGS= -I.
SRCFILES= matrix_io.c ## or perhaps $(wildcard *.c)
OBJFILES= $(patsubst %.c, %.o, $(SRCFILES))
PROGFILES= $(patsubst %.c, %, $(SRCFILES))

.PHONY: all clean

all: libmatrix_io.a

libmatrix_io.a: matrix_io.o
	ar ruv libmatrix_io.a matrix_io.o
	ranlib libmatrix_io.a

matrix_io.o: matrix_io.c

clean:
	$(RM) $(OBJFILES) $(PROGFILES) *~
