# MatrixIO
MatrixIO is a C library for interfacing with MATLAB and HDF5 matrix files.

## Author
Satyam Ghodasara, sghodas@gmail.com

## License
MatrixIO is available under the MIT license. See the LICENSE file for more info.
