//
//  matrix_io.c
//  MatrixIO
//
//  Satyam Ghodasara
//

#include <stdlib.h>
#include <string.h>
#include "hdf5.h"
#include "matrix_io.h"

//  Helpers - forward declarations

mwIndex rowMajIdxFromColMajIdx(const mwIndex colMajIdx, const mwIndex width, const mwIndex height);
mwIndex colMajIdxFromRowMajIdx(const mwIndex rowMajIdx, const mwIndex width, const mwIndex height);

double *rowMajMatrixFromColMajMatrix(const double *colMajMatrix, const mwIndex width, const mwIndex height);
double *colMajMatrixFromRowMajMatrix(const double *rowMajMatrix, const mwIndex width, const mwIndex height);

//  Reading from matrix files

mxArray *getMatrixInMATFile(const char *fileName, const char *matrixName)
{
    MATFile *matrixFile = matOpen(fileName, "r");
    mxArray *matrix = matGetVariable(matrixFile, matrixName);
    matClose(matrixFile);
    return matrix;
}

mxArray *getMatrixInHDF5File(const char *fileName, const char *matrixName)
{
    hid_t fileID = H5Fopen(fileName, H5F_ACC_RDWR, H5P_DEFAULT);
    hid_t datasetID = H5Dopen2(fileID, matrixName, H5P_DEFAULT);

	hid_t dataspaceID = H5Dget_space(datasetID);
	hsize_t dims[2];
	H5Sget_simple_extent_dims(dataspaceID, dims, NULL);

	double *dsetData = (double *)malloc(sizeof(double) * dims[0] * dims[1]);
	H5Dread(datasetID, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, dsetData);
	H5Dclose(datasetID);

	mxArray *matrix = mxCreateDoubleMatrix(dims[1], dims[0], mxREAL);
	mxSetData(matrix, dsetData);
	return matrix;
}

mxArray *getColInMatrix(const mxArray *matrix, const int colIdx)
{
    double *matrixData = mxGetPr(matrix);
    int nRows = mxGetM(matrix);
    double *matrixColData = (double *)mxMalloc(sizeof(double) * nRows);
    for (int i = colIdx * nRows; i < colIdx * nRows + nRows; i++) {
        matrixColData[i % nRows] = matrixData[i];
    }

    mxArray *matrixCol = mxCreateDoubleMatrix(nRows, 1, mxREAL);
    mxSetPr(matrixCol, matrixColData);
    return matrixCol;
}

mxArray *getRowInMatrix(const mxArray *matrix, const int rowIdx)
{
    double *matrixData = mxGetPr(matrix);
	int nRows = mxGetM(matrix);
	int nCols = mxGetN(matrix);
	double *matrixRowData = (double *)mxMalloc(sizeof(double) * nCols);
	for (int i = rowIdx; i < nRows * nCols; i += nRows) {
		matrixRowData[i / nRows] = matrixData[i];
	}

	mxArray *matrixRow = mxCreateDoubleMatrix(1, nCols, mxREAL);
	mxSetPr(matrixRow, matrixRowData);
	return matrixRow;
}

//  Writing matrix files

int putMatrixInMATFile(const char *fileName, const char *matrixName, const mxArray *matrix)
{
    MATFile *matrixFile = matOpen(fileName, "w");
	int err = matPutVariable(matrixFile, matrixName, matrix);
	matClose(matrixFile);
	return err;
}

int putMatrixInHDF5File(const char *fileName, const char *matrixName, const mxArray *matrix)
{
    char *datasetName = (char *)malloc(strlen(matrixName) + 2);
	datasetName[0] = '/';
	datasetName[1] = '\0';
	strcat(datasetName, matrixName);

	hid_t fileID = H5Fcreate(fileName, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	hsize_t dims[] = { mxGetM(matrix), mxGetN(matrix) };
	hid_t dataspaceID = H5Screate_simple(2, dims, NULL);
	hid_t datasetID = H5Dcreate2(fileID, datasetName, H5T_NATIVE_DOUBLE, dataspaceID, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	int err = H5Dwrite(datasetID, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, mxGetPr(matrix));

	H5Dclose(datasetID);
	H5Sclose(dataspaceID);
	H5Fclose(fileID);

    return err;
}

//  Helpers

mwIndex rowMajIdxFromColMajIdx(const mwIndex colMajIdx, const mwIndex width, const mwIndex height)
{
    mwIndex row = colMajIdx % height;
	mwIndex col = colMajIdx / height;
	return row * width + col;
}

mwIndex colMajIdxFromRowMajIdx(const mwIndex rowMajIdx, const mwIndex width, const mwIndex height)
{
    return rowMajIdxFromColMajIdx(rowMajIdx, height, width);
}

double *rowMajMatrixFromColMajMatrix(const double *colMajMatrix, const mwIndex width, const mwIndex height)
{
    mwIndex colMajIdx, rowMajIdx;
	double *rowMajMatrix = (double *)malloc(sizeof(double) * width * height);

	for (colMajIdx = 0; colMajIdx < width * height; colMajIdx++) {
		rowMajIdx = rowMajIdxFromColMajIdx(colMajIdx, width, height);
		rowMajMatrix[rowMajIdx] = colMajMatrix[colMajIdx];
	}
	return rowMajMatrix;
}

double *colMajMatrixFromRowMajMatrix(const double *rowMajMatrix, const mwIndex width, const mwIndex height)
{
    mwIndex colMajIdx, rowMajIdx;
	double *colMajMatrix = (double *)malloc(sizeof(double) * width * height);

	for (rowMajIdx = 0; rowMajIdx < width * height; rowMajIdx++) {
		colMajIdx = colMajIdxFromRowMajIdx(rowMajIdx, width, height);
		colMajMatrix[colMajIdx] = rowMajMatrix[rowMajIdx];
	}
	return colMajMatrix;
}
